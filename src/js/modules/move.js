(function($) {
    'use strict';

    const $box = $('.tab-move-box');
    const $image = $('.tab-move-image');

    const speedRate = 1.5;

    function getAnimationParams() {
        return {
            'topLeft': {
                top: `0`,
                left: `0`
            },
            'topCenter': {
                top: `0`,
            },
            'topRight': {
                top: `0`,
                left: `${$box.outerWidth() - $image.outerWidth()}px`
            },
            'centerLeft': {
                left: `0`
            },
            'centerRight': {
                left: `${$box.outerWidth() - $image.outerWidth()}px`
            },
            'bottomLeft': {
                top: `${$box.outerHeight() - $image.outerHeight()}px`,
                left: `0`
            },
            'bottomCenter': {
                top: `${$box.outerHeight() - $image.outerHeight()}px`,
            },
            'bottomRight': {
                top: `${$box.outerHeight() - $image.outerHeight()}px`,
                left: `${$box.outerWidth() - $image.outerWidth()}px`
            }
        }
    };

    function getDirection(x, y) {
        if (x <= $box.outerWidth() / 3 && y <= $box.outerHeight() / 3) {
            return 'topLeft';
        }

        if (x > $box.outerWidth() / 3 && x <= $box.outerWidth() * 2 / 3 && y <= $box.outerHeight() / 3) {
            return 'topCenter';
        }

        if (x > $box.outerWidth() * 2 / 3 && y <= $box.outerHeight() / 3) {
            return 'topRight';
        }

        if (x <= $box.outerWidth() / 3 && y > $box.outerHeight() / 3 && y <= $box.outerHeight() * 2 / 3) {
            return 'centerLeft';
        }

        if (x > $box.outerWidth() / 3 && x <= $box.outerWidth() * 2 / 3 && y > $box.outerHeight() / 3 && y <= $box.outerHeight() * 2 / 3) {
            return 'center';
        }

        if (x > $box.outerWidth() * 2 / 3 && y > $box.outerHeight() / 3 && y <= $box.outerHeight() * 2 / 3) {
            return 'centerRight';
        }

        if (x <= $box.outerWidth() / 3 && y > $box.outerHeight() * 2 / 3) {
            return 'bottomLeft';
        }

        if (x > $box.outerWidth() / 3 && x <= $box.outerWidth() * 2 / 3 && y > $box.outerHeight() * 2 / 3) {
            return 'bottomCenter';
        }

        if (x > $box.outerWidth() * 2 / 3 && y > $box.outerHeight() * 2 / 3) {
            return 'bottomRight';
        }
    };

    function speedCalc(dir) {
        return Math.ceil(dir * speedRate);
    };

    function getSpeed (direction) {
        const top = -$image.position().top;
        const right = $image.outerWidth() - $box.outerWidth() + $image.position().left;
        const bottom = $image.outerHeight() - $box.outerHeight() + $image.position().top;
        const left = -$image.position().left;

        if (direction === 'topLeft') {
           return left > top ? speedCalc(left) : speedCalc(top);
        }

        if (direction === 'topCenter') {
            return speedCalc(top);
        }

        if (direction === 'topRight') {
            return right > top ? speedCalc(right) : speedCalc(top);
        }

        if (direction === 'centerLeft') {
            return speedCalc(left);
        }

        if (direction === 'centerRight') {
            return speedCalc(right);
        }

        if (direction === 'bottomLeft') {
            return left > bottom ? speedCalc(left) : speedCalc(bottom);
        }

        if (direction === 'bottomCenter') {
            return speedCalc(bottom);
        }

        if (direction === 'bottomRight') {
            return right > bottom ? speedCalc(right) : speedCalc(bottom);
        }
    }

    function makeDirectionChecker(x, y) {
        let direction = getDirection(x, y);

        return function(x, y) {
            if (direction !== getDirection(x, y)) {
                direction = getDirection(x, y);
                return true;
            }

            return false;
        }
    }

    $box.on('mouseenter', function (e) {
        let x = e.offsetX;
        let y = e.offsetY;

        const isDirectionChanged = makeDirectionChecker(x, y);
        const animations = getAnimationParams();

        $box.on('mousemove', function (e) {
           let x = e.pageX - $box.position().left;
           let y = e.pageY - $box.position().top;

           if (isDirectionChanged(x, y)) {
               const direction = getDirection(x, y);
               if (direction === 'center') {
                   $image.stop();
                   return;
               }

               $image.stop();
               $image.animate(animations[direction], getSpeed(direction), 'linear');

           }
        })

        $box.on('mouseleave', function () {
            $image.stop();
            $box.off('mouseleave mousemove');
        });

        const direction = getDirection(x, y);
       $image.animate(animations[direction], getSpeed(direction), 'linear');
    })
})(jQuery);
