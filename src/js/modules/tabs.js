(function($) {
    'use strict';

    /**
     * Табы
     */
    $.fn.tabs = function () {
        var $self = $(this);
        var $tabHeaders = $self.find('.js-tab-header').filter(function (index, el) {
            return $(el).parentsUntil($self).length === 2;
        });
        var $tabContent = $self.find('.js-tab-content').filter(function (index, el) {
            return $(el).parentsUntil($self).length === 1;
        });

        /**
         * Активация таба по его индексу
         * @param {Number} index - индекс таба, который нужно активировать
         */
        var selectTab = function (index) {
            $tabHeaders.removeClass('active active-mobile').eq(index).addClass('active active-mobile');
            $('.tab-header-mobile').html($tabHeaders.eq(index).html());

            $tabContent.removeClass('active').eq(index).addClass('active');
            if ($tabContent.eq(index).hasClass('tab-drag-box')) {
                dragAndDropInit();
            };
        };

        /**
         * Инициализаиця
         */
        var init = function () {
            selectTab(0);

            // Обработка событий
            $tabHeaders.on('click', function () {
                selectTab($(this).index());
                if ($(window).width() < 768) {
                    $('.tab-header-wrap').hide();
                    $('.tab-header-mobile').toggleClass('opened');
                }
            });
        };

        init();

        this.selectTab = selectTab;

        return this;
    };

    // Инициализируем табы на всех блоках с классом 'js-tabs'
    $('.js-tabs').each(function () {
        $(this).data('tabs', $(this).tabs());
    });

    $('.tab-header-mobile').on('click', function() {
        $('.tab-header-mobile').toggleClass('opened');
        $('.tab-header-wrap').slideToggle();
    });

    $(window).resize(function() {
        if ($(window).width() > 767) {
            $('.tab-header-wrap').removeAttr('style');
            $('.tab-header-mobile').removeClass('opened');
        }
    })
})(jQuery);
