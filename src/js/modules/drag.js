(function($) {
    'use strict';

    const $wrap = $('.tab-content-wrap');
    const $box = $('.tab-drag-box');
    const $image = $('.tab-drag-image');

    function dragAndDropInit() {
        const x1 = $box.offset().left - ($image.outerWidth() - $box.outerWidth());
        const x2 = $box.offset().left;
        const y1 = $box.offset().top - ($image.outerHeight() - $box.outerHeight());
        const y2 = $box.offset().top;

        $('.tab-drag-image').draggable({
            containment: [x1, y1, x2, y2]
        });
    }

    window.dragAndDropInit = dragAndDropInit;
})(jQuery);
