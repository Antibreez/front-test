(function($) {
    'use strict';

    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 99) {
            $('.header').not('.fixed').addClass('fixed');
        } else {
            $('.header').removeClass('fixed');
        }
    })
})(jQuery);
